DROP TABLE IF EXISTS "task_follow";
CREATE TABLE "task_follow" (
    "follow_id" SERIAL PRIMARY KEY,
    "user_id" INT REFERENCES "user_info" ("user_id"),
    "task_id" INT REFERENCES "task_post" ("task_id"),
    "following" BOOL,
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL
);