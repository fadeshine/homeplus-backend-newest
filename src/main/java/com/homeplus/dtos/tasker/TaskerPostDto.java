package com.homeplus.dtos.tasker;

import com.homeplus.models.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskerPostDto {
    private Long user_id;

    private UserEntity userEntity;

    private String title;

    private String skills_description;

    private String introduction;

    private String certifications;

    private String category;

    private String bank_bsb;

    private String bank_account;

    private final OffsetDateTime created_time = OffsetDateTime.now();

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
