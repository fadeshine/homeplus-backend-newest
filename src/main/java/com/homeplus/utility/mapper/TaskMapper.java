package com.homeplus.utility.mapper;

import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.task.TaskPostDto;
import com.homeplus.dtos.task.TaskPutDto;
import com.homeplus.models.TaskEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskMapper {

    TaskEntity postDtoToEntity(TaskPostDto taskPostDto);

    TaskEntity putDtoToEntity(TaskPutDto taskPutDto);

    TaskGetDto fromEntity(TaskEntity taskEntity);

    TaskGetDto taskToTaskGetDto(TaskEntity taskEntity);

    void copy(TaskPutDto taskPutDto, @MappingTarget TaskEntity taskEntity);
}
