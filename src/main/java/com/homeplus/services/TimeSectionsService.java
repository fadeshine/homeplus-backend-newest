package com.homeplus.services;

import com.homeplus.models.TimeSectionsEntity;
import com.homeplus.repositories.TimeSectionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TimeSectionsService {

    private final TimeSectionsRepository timeSectionsRepository;

    public TimeSectionsEntity getTimeById(Long id) {
        return timeSectionsRepository.findById(id).get();
    }
}
