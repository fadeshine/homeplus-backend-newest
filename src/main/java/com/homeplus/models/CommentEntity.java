package com.homeplus.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "task_post_comment")
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "task_id")
    private TaskEntity taskEntity;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "created_time", nullable = false)
    private OffsetDateTime created_time;

    @Column(name = "updated_time")
    private OffsetDateTime updated_time;
}
