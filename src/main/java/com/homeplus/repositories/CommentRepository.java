package com.homeplus.repositories;

import com.homeplus.dtos.comment.CommentGetDto;
import com.homeplus.models.CommentEntity;
import com.homeplus.models.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {

    @Query("select c from CommentEntity as c where c.taskEntity in (:taskEntity)")
    List<CommentEntity> findCommentsByTask(TaskEntity taskEntity);
}
