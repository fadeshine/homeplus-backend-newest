package com.homeplus.repositories;

import com.homeplus.models.ConfirmationTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface ConfirmationTokenRepository
        extends JpaRepository<ConfirmationTokenEntity, Long> {

    Optional<ConfirmationTokenEntity> findByToken(String token);

    Optional<ConfirmationTokenEntity> findByEmail(String email);

    @Modifying
    @Query("update ConfirmationTokenEntity c set c.token = null, c.createTime = null, c.expireTime = null where c.email = :email")
    int updateTokenToNull(String email);

//    Optional<ConfirmationTokenEntity> findByEmail(String email);



}
